"""
  Author:   Lukas Heinrich
  Purpose:  Perform fit toy data to the input signal plus mock background generated from a falling exponential pdf. Report the fit result.
  Revision History:
        - 190812 (Danika MacDonell): 
            * create mock background and data on the spot, rather than reading them in, for transparency
            * do scaling in this step, rather than previous
            * read in signal as txt rather than json file (to jive with preliminary tutorial exercise)
            * plot data, background, & signal
            * plot mu scan rather than outputting limit as json
            
  Sample run command: python run_fit.py -i selected.txt -o limit.png -p hists.png -c 44.837 -s 6813.025800 -k 1.0 -f 1.0 -l 140.1
"""

import pyhf
import json
import click
import numpy as np
import scipy.stats
import matplotlib.pyplot as plt
plt.rc('xtick', labelsize=16)
plt.rc('ytick', labelsize=16)

@click.command()
@click.option('-i', '--inputfile')
@click.option('-o','--outputfile', default = 'fitresult.json')
@click.option('-p', '--plotfile', default = 'recast.png')
@click.option('-c', '--xsection')
@click.option('-s', '--sumofweights')
@click.option('-k', '--kfactor')
@click.option('-f', '--filterfactor')
@click.option('-l', '--luminosity')

def main(inputfile, xsection, sumofweights, kfactor, filterfactor, luminosity, outputfile, plotfile):
  
    # Calculate the overall scaling constant for the signal histogram
    scaling = float(xsection) * float(filterfactor) * float(kfactor) * float(luminosity) / float(sumofweights)
  
    # Collect the signal content and edges from the input file
    f_input = open(inputfile)
    edges = np.fromstring(f_input.readline(), dtype=float, sep=" ")
    signal = np.fromstring(f_input.readline(), dtype=float, sep=" ") * scaling
    bin_width = edges[1]-edges[0]
    
    # Sample the background from a falling exponential pdf
    background, edges = np.histogram(np.random.exponential(scale = 150, size = 50000),bins = edges)
    background_uncert = np.sqrt(background)     # Poisson statistics
    
    # Generate toy data from poisson variations about the background bin amplitudes
    data = scipy.stats.poisson(background).rvs()
    
    # Plot the background, signal, and data
    plt.bar(edges[:-1],signal+background, width = bin_width, facecolor = 'red', label="sig+bkg")
    plt.bar(edges[:-1],background, width = bin_width, facecolor = 'steelblue', label="bkg")
    plt.errorbar(edges[:-1], data, c = 'k',zorder = 10, yerr = np.sqrt(data), fmt = 'o', label="data", markersize=3)
    plt.legend(prop={'size': 16})
    plt.xlabel("Dijet Invariant Mass (GeV)", fontsize=16)
    plt.ylabel("Events / %.0f GeV"%(edges[1]-edges[0]), fontsize=16)
    plt.tight_layout()
    plt.savefig(plotfile)
    plt.close()

    # build likelihood
    pdf = pyhf.simplemodels.hepdata_like(
        signal.tolist(),
        background.tolist(),
        background_uncert.tolist(),
    )
    obs,expset = pyhf.utils.hypotest(
        1.0,
        data.tolist() + pdf.config.auxdata,
        pdf,
        return_expected_set = True
    )
    
    results = []
    muset = np.linspace(0,1.2,21)
    for mu in muset:
        obs, exp = pyhf.utils.hypotest(mu, data.tolist() + pdf.config.auxdata, pdf, return_expected_set = True)
        results.append((obs,exp))

    observed = [r[0][0] for r in results]
    down_2sig = [r[1][0][0] for r in results]
    up_2sig = [r[1][-1][0] for r in results]
    down_1sig = [r[1][1][0] for r in results]
    up_1sig = [r[1][-2][0] for r in results]
    expected = [r[1][2][0] for r in results]

    fig = plt.figure(figsize=(7, 5))
    ax = fig.add_subplot(1, 1, 1)
    ax.set_title('Hypothesis Tests', fontsize=16)
    ax.set_ylabel("CL$_s$", fontsize=16)
    ax.set_xlabel("$\mu$", fontsize=16)
    plt.tight_layout()
    ax.plot(muset, observed, c="black", label="observed")
    ax.plot(muset, expected, c="cyan", ls="dashed", label="expected")
    ax.fill_between(muset, down_2sig, up_2sig, facecolor = "yellow", label="expected $\pm$ 2$\sigma$")
    ax.fill_between(muset, down_1sig, up_1sig, facecolor = "green", label="expected $\pm$ 1$\sigma$")
    ax.axhline(0.05, c="red")
    ax.set_xlim(0, 1.2)
    ax.legend(prop={'size': 16})
    plt.savefig(outputfile)
    
    """
    result = {
        'obs': obs.tolist()[0],
        'exp': [e.tolist()[0] for e in expset]
    } 
    result = json.dumps(result, indent = 4)
    click.secho(result)
    with open(outputfile,'w') as f:
        f.write(result)
    """

if __name__ == '__main__':
    main()
